defmodule Jaminal.Factory do
  use ExMachina.Ecto, repo: Jaminal.Repo
  alias Jaminal.Trucks.{Applicant, Permit, Truck}

  def applicant_factory(attrs) do
    %Applicant{
      name: sequence("applicant")
    }
    |> merge_attributes(attrs)
    |> evaluate_lazy_attributes()
  end

  def permit_factory(attrs) do
    %Permit{
      permit_id: sequence("permit_"),
      status: Enum.random(Permit.status()),
      trucks: [build(:truck)],
      applicant: build(:applicant)
    }
    |> merge_attributes(attrs)
    |> evaluate_lazy_attributes()
  end

  def truck_factory(attrs) do
    %Truck{
      address: sequence("address"),
      location_id: System.unique_integer([:positive]),
      facility_type: Enum.random(Truck.facilities())
    }
    |> merge_attributes(attrs)
    |> evaluate_lazy_attributes()
  end
end
