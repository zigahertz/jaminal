defmodule JaminalWeb.SchemaTest do
  use JaminalWeb.ConnCase, async: true

  @applicants_query """
    {
      applicants {
        name
      }
    }
  """

  defp query(conn, opts) do
    req = %{
      "query" => Keyword.get(opts, :query),
      "variables" => Keyword.get(opts, :variables, nil)
    }

    conn
    |> post("/api", req)
    |> json_response(200)
  end

  defp data(resp, key), do: get_in(resp, ["data", key])

  test "query for applicants", %{conn: conn} do
    insert(:applicant)

    assert applicants = query(conn, query: @applicants_query) |> data("applicants")
    assert is_list(applicants)
    assert applicants != []
    assert Enum.filter(applicants, &Map.has_key?(&1, "name")) != []
    assert Enum.filter(applicants, &Map.has_key?(&1, "other")) == []
  end

  @truck_query """
    query($filters: Filters) {
      truckCount(filters: $filters)
      trucks(filters: $filters) {
        permit {
          status
        }
      }
    }

  """

  @count_query """
    {
      truckCount
    }
  """

  test "can query with multiple arguments", %{conn: conn} do
    insert(:permit, %{status: :approved, trucks: [build(:truck, %{facility_type: :push_cart})]})
    insert(:permit, %{status: :approved, trucks: [build(:truck, %{facility_type: :truck})]})

    Enum.each(1..4, fn _ ->
      insert(:permit, %{status: :suspend, trucks: [build(:truck, %{facility_type: :push_cart})]})
    end)

    assert simple_query = query(conn, query: @truck_query)
    assert count = query(conn, query: @count_query)
    assert length(get_in(simple_query, ["data", "trucks"])) == data(count, "truckCount")

    assert approved =
              query(conn, query: @truck_query, variables: %{filters: %{permit_status: "APPROVED"}}) |> data("trucks")

    assert length(approved) == 2

    assert suspended =
             query(conn, query: @truck_query, variables: %{filters: %{permit_status: "SUSPEND"}}) |> data("trucks")

    assert length(suspended) == 4

    assert carts =
             query(conn, query: @truck_query, variables: %{filters: %{facility_type: "PUSH_CART"}}) |> data("trucks")

    assert length(carts) == 5

    assert approved_cart =
             query(conn, query: @truck_query, variables: %{filters: %{permit_status: "APPROVED", facility_type: "PUSH_CART"}})

    assert length(data(approved_cart, "trucks")) == 1
    assert data(approved_cart, "truckCount") == 1
  end
end
