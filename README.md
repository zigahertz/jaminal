# Jaminal: GraphQL-based Food Truck API

## getting started

- install dependencies with `mix deps.get`
- create and migrate your local database with `mix ecto.setup`
- hydrate the DB using CSV data with `mix ecto.hydrate`
- start Phoenix endpoint with `iex -S mix phx.server`
- visit the GraphQL API at http://localhost:4000/api/graphiql

## schema

This API provides information about food trucks in San Francisco. I modeled the data using 3 Ecto schemas:

- Applicants
- Permits
- Trucks

To demonstrate query filtering, I added query arguments for permit status and truck facility type.

### example queries

```
query {
  trucks(filters: {facilityType: TRUCK, permitStatus: APPROVED}) {
    address
    facilityType
    latitude
    longitude
    permit {
      status
      permitId
      approvedAt
    }
    applicant {
      name
    }
  }
}
```

```
query {
  applicants {
    name
    trucks {
      facilityType
      foodItems
    }
    permits {
      permitId
      expiresAt
      receivedAt
      approvedAt
      status
    }
  }
}
```

```
query {
  truckCount(filters: {permitStatus: REQUESTED})
}
```

## notes & comments

Tests are in `JaminalWeb.SchemaTest`.

I used this project to demonstrate several things:

- modeling and parsing data for insertion to DB
- use of Ecto, model relationships
- learning Absinthe (I have created GraphQL servers with Node but not with Elixir)
- general patterns and code structure
- because of the relative simplicity of the schema and assignment, I opted to preload associations in a single query (for example, see `Jaminal.Trucks.list_applicants/0`), versus using something like `dataloader` to dynamically and efficiently load associated data.

Also, I ignored the zipcode column in the CSV because it's incorrect for SF-based locations.
