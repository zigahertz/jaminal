defmodule Jaminal.Repo.Migrations.CreateLocations do
  use Ecto.Migration

  def change do
    create_facility_type = "CREATE TYPE facility AS ENUM ('truck', 'push_cart')"
    drop_facility_type = "DROP TYPE facility"

    execute(create_facility_type, drop_facility_type)

    create table(:trucks) do
      add :address, :string
      add :block, :string
      add :cnn, :integer
      add :dayshours, :string
      add :facility_type, :facility
      add :food_items, :text
      add :latitude, :decimal
      add :location_description, :string
      add :location_id, :integer, null: false
      add :longitude, :decimal
      add :lot, :string
      add :prior_permit, :integer
      add :schedule, :string
      add :x, :decimal
      add :y, :decimal

      add :permit_id, references(:permits, on_delete: :nothing), null: false

      timestamps()
    end

    create index(:trucks, [:permit_id])
    create index(:trucks, [:latitude, :longitude])
    create index(:trucks, [:address])
  end
end
