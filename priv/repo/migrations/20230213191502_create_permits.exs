defmodule Jaminal.Repo.Migrations.CreatePermits do
  use Ecto.Migration

  def change do
    create_status_type =
      "CREATE TYPE permit_status AS ENUM ('approved', 'requested', 'suspend', 'expired')"

    drop_status_type = "DROP TYPE permit_status"

    execute(create_status_type, drop_status_type)

    create table(:permits) do
      add :permit_id, :string, null: false
      add :status, :permit_status
      add :approved_at, :date
      add :expires_at, :date
      add :received_at, :date
      add :applicant_id, references(:applicants, on_delete: :nothing), null: false

      timestamps()
    end

    create index(:permits, [:applicant_id])
    create unique_index(:permits, [:permit_id])
  end
end
