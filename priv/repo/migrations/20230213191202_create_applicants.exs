defmodule Jaminal.Repo.Migrations.CreateApplicants do
  use Ecto.Migration

  def change do
    create table(:applicants) do
      add :name, :string, null: false

      timestamps()
    end

    create unique_index(:applicants, [:name])
  end
end
