defmodule Jaminal.ParseData do
  alias Jaminal.Repo
  alias Jaminal.Trucks.Applicant
  import Jaminal.Util, only: [atomize: 1, int: 1, stringify: 1, to_key: 1]

  def insert_data(file \\ "data.csv") do
    file
    |> File.stream!()
    |> CSV.decode!(headers: true)
    |> Enum.reduce([], &parse/2)
    |> Enum.map(&insert_applicant/1)
  end

  defp parse(%{"Applicant" => applicant} = truck, acc) do
    applicant = atomize(applicant)

    if Keyword.has_key?(acc, applicant) do
      permits = Keyword.get(acc, applicant)

      Keyword.put(acc, applicant, build_permit(truck, permits))
    else
      [{applicant, build_permit(truck)} | acc]
    end
  end

  defp insert_applicant({name, permits}) do
    %Applicant{}
    |> Applicant.changeset(%{name: stringify(name), permits: Keyword.values(permits)})
    |> Repo.insert()
  end

  defp build_permit(%{"permit" => permit_id} = attrs, permits \\ []) do
    permit_id = atomize(permit_id)

    case Keyword.get(permits, permit_id) do
      %{trucks: trucks} = p ->
        Keyword.put(
          permits,
          permit_id,
          Map.merge(p, %{trucks: [build_truck(attrs) | trucks]})
        )

      nil ->
        permit = %{
          permit_id: attrs["permit"],
          approved_at: parse_date(attrs["Approved"]),
          expires_at: parse_date(attrs["ExpirationDate"]),
          received_at: parse_date(attrs["Received"]),
          status: to_key(attrs["Status"]),
          trucks: [build_truck(attrs)]
        }

        Keyword.put(permits, permit_id, permit)
    end
  end

  defp build_truck(attrs) do
    %{
      address: attrs["Address"],
      facility_type: to_key(attrs["FacilityType"]),
      food_items: attrs["FoodItems"],
      latitude: attrs["Latitude"],
      longitude: attrs["Longitude"],
      schedule: attrs["Schedule"],
      block: stringify(attrs["block"]),
      lot: stringify(attrs["lot"]),
      cnn: attrs["cnn"],
      location_id: attrs["locationid"],
      location_description: attrs["LocationDescription"],
      x: attrs["X"],
      y: attrs["Y"]
    }
  end

  @doc """
  distinguishes between YYYYMMDD (length = 8) and MM/DD/YYYY hh:mm:ss AM (length = 22)
  """
  @spec parse_date(String.t()) :: %Date{}
  def parse_date(date) when is_binary(date) do
    case String.split(date, "", trim: true) |> length do
      0 ->
        nil

      8 ->
        {year, remainder} = String.split_at(date, 4)
        {month, day} = String.split_at(remainder, 2)

        Date.new!(int(year), int(month), int(day))

      22 ->
        [month, day, year] =
          date
          |> String.split(" ")
          |> Enum.at(0)
          |> String.split("/")

        Date.new!(int(year), int(month), int(day))

      _ ->
        nil
    end
  end
end
