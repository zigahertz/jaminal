defmodule Jaminal.Trucks do
  import Ecto.Query
  alias Jaminal.Repo
  alias Jaminal.Trucks.{Applicant, Permit, Truck}

  def list_applicants do
    from(a in Applicant,
      left_join: p in assoc(a, :permits),
      left_join: t in assoc(p, :trucks),
      preload: [permits: p, trucks: t]
    )
    |> Repo.all()
  end

  def list_permits do
    from(p in Permit,
      left_join: a in assoc(p, :applicant),
      left_join: t in assoc(p, :trucks),
      preload: [applicant: a, trucks: t]
    )
    |> Repo.all()
  end

  def list_trucks(args) do
    trucks_query(args) |> Repo.all()
  end

  def count_trucks(args) do
    trucks_query(args) |> Repo.aggregate(:count)
  end

  defp trucks_query(args) do
    from(t in Truck,
      left_join: p in assoc(t, :permit),
      left_join: a in assoc(p, :applicant),
      preload: [permit: p, applicant: a]
    )
    |> filter_trucks(args)
  end

  def filter_trucks(query, %{filters: filters}), do: Enum.reduce(filters, query, &build_query/2)
  def filter_trucks(query, %{}), do: query

  defp build_query({:permit_status, status}, query) do
    from(query, where: ^dynamic([t, p], p.status == ^status))
  end

  defp build_query({:facility_type, type}, query) do
    from(query, where: ^dynamic([t], t.facility_type == ^type))
  end
end
