defmodule Jaminal.Util do
  def atomize(s) when is_binary(s), do: String.to_atom(s)
  def int(s) when is_binary(s), do: String.to_integer(s)
  def stringify(i) when is_integer(i), do: Integer.to_string(i)
  def stringify(s) when is_binary(s), do: s
  def stringify(a) when is_atom(a), do: Atom.to_string(a)

  def to_key(""), do: nil

  def to_key(str) do
    str
    |> String.downcase()
    |> String.replace(" ", "_")
    |> atomize()
  end
end
