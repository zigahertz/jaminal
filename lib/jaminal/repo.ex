defmodule Jaminal.Repo do
  use Ecto.Repo,
    otp_app: :jaminal,
    adapter: Ecto.Adapters.Postgres
end
