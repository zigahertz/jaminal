defmodule Jaminal.Trucks.Applicant do
  use Ecto.Schema
  import Ecto.Changeset
  alias Jaminal.Trucks.Permit

  schema "applicants" do
    field :name, :string
    has_many :permits, Permit
    has_many :trucks, through: [:permits, :trucks]

    timestamps()
  end

  @doc false
  def changeset(applicant, attrs) do
    applicant
    |> cast(attrs, [:name])
    |> validate_required([:name])
    |> unique_constraint(:name)
    |> cast_assoc(:permits)
  end
end
