defmodule Jaminal.Trucks.Truck do
  use Ecto.Schema
  import Ecto.Changeset
  alias Jaminal.Trucks.Permit

  @attrs ~w(address block cnn dayshours facility_type food_items latitude location_description location_id longitude lot prior_permit schedule x y)a
  @facilities ~w(truck push_cart)a

  schema "trucks" do
    field :address, :string
    field :block, :string
    field :cnn, :integer
    field :dayshours, :string
    field :facility_type, Ecto.Enum, values: @facilities
    field :food_items, :string
    field :latitude, :decimal
    field :location_description, :string
    field :location_id, :integer
    field :longitude, :decimal
    field :lot, :string
    field :prior_permit, :integer
    field :schedule, :string
    field :x, :decimal
    field :y, :decimal

    belongs_to :permit, Permit
    has_one :applicant, through: [:permit, :applicant]

    timestamps()
  end

  @doc false
  def changeset(truck, attrs) do
    truck
    |> cast(attrs, @attrs)
    |> validate_required([:location_id])
  end

  def facilities, do: @facilities
end
