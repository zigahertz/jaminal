defmodule Jaminal.Trucks.Permit do
  use Ecto.Schema
  import Ecto.Changeset

  alias Jaminal.Trucks.{Applicant, Truck}

  @attrs ~w(approved_at expires_at received_at permit_id status)a
  @status ~w(requested approved suspend expired)a

  schema "permits" do
    field :approved_at, :date
    field :expires_at, :date
    field :received_at, :date
    field :permit_id, :string
    field :status, Ecto.Enum, values: @status

    belongs_to :applicant, Applicant
    has_many :trucks, Truck

    timestamps()
  end

  def status(), do: @status

  @doc false
  def changeset(permit, attrs) do
    status()

    permit
    |> cast(attrs, @attrs)
    |> validate_required([:permit_id, :status])
    |> unique_constraint(:permit_id)
    |> cast_assoc(:trucks)
  end
end
