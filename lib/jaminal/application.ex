defmodule Jaminal.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      Jaminal.Repo,
      # Start the Telemetry supervisor
      JaminalWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Jaminal.PubSub},
      # Start the Endpoint (http/https)
      JaminalWeb.Endpoint
      # Start a worker by calling: Jaminal.Worker.start_link(arg)
      # {Jaminal.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Jaminal.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    JaminalWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
