defmodule JaminalWeb.Schema.Types do
  use Absinthe.Schema.Notation

  import_types(Absinthe.Type.Custom)

  enum :permit_status do
    value(:requested)
    value(:approved)
    value(:suspend)
    value(:expired)
  end

  enum :facility_type do
    value(:push_cart)
    value(:truck)
  end

  input_object :filters do
    field :permit_status, :permit_status
    field :facility_type, :facility_type
  end

  object :applicant do
    field :name, non_null(:string)
    field :permits, list_of(:permit)
    field :trucks, list_of(:truck)
  end

  object :permit do
    field :permit_id, non_null(:string)
    field :status, non_null(:permit_status)
    field :approved_at, :date
    field :expires_at, :date
    field :received_at, :date
    field :applicant, :applicant
    field :trucks, list_of(:truck)
  end

  object :truck do
    field :location_id, non_null(:integer)
    field :address, :string
    field :block, :string
    field :cnn, :integer
    field :dayshours, :string
    field :facility_type, :facility_type
    field :food_items, :string
    field :latitude, :string
    field :location_description, :string
    field :longitude, :string
    field :lot, :string
    field :prior_permit, :integer
    field :schedule, :string
    field :x, :string
    field :y, :string
    field :applicant, non_null(:applicant)
    field :permit, non_null(:permit)
  end
end
