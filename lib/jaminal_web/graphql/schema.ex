defmodule JaminalWeb.Schema do
  use Absinthe.Schema
  alias JaminalWeb.Resolvers

  import_types(JaminalWeb.Schema.Types)

  query do
    @desc "get food trucks"
    field :trucks, non_null(list_of(:truck)) do
      arg(:filters, :filters)
      resolve(&Resolvers.list_trucks/3)
    end

    @desc "count trucks"
    field :truck_count, :integer do
      arg(:filters, :filters)
      resolve(&Resolvers.count_trucks/3)
    end

    @desc "get all applicants"
    field :applicants, list_of(:applicant) do
      resolve(&Resolvers.list_applicants/3)
    end

    @desc "get all permits"
    field :permits, list_of(:permit) do
      arg(:status, :permit_status)
      resolve(&Resolvers.list_permits/3)
    end
  end
end
