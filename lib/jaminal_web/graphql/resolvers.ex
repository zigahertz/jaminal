defmodule JaminalWeb.Resolvers do
  alias Jaminal.Trucks

  def list_applicants(_, _args, _) do
    {:ok, Trucks.list_applicants()}
  end

  def list_permits(_, _, _) do
    {:ok, Trucks.list_permits()}
  end

  def list_trucks(_, args, _) do
    {:ok, Trucks.list_trucks(args)}
  end

  def count_trucks(_, args, _) do
    {:ok, Trucks.count_trucks(args)}
  end
end
